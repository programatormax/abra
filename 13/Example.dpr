program Example;

{$APPTYPE CONSOLE}

uses

  Generics.Collections;

type

  TKnedlik = class
    Cena: Currency;
    Chutny: Boolean;
    Vyborny: Boolean;
    ZkouselToBabis: Boolean; // :-)
  end;

  TSeznam = class(TObjectList<TKnedlik>);

var

  Seznam: TSeznam;
  Knedlik: TKnedlik;

begin
 Seznam:=TSeznam.Create;
 Knedlik:=TKnedlik.Create;
 Knedlik.Chutny:=True;
 Knedlik.Cena:=100;
 Knedlik.Vyborny:=True;
 Knedlik.ZkouselToBabis:=False;     //Ne :-)
 Seznam.Add(Knedlik);
 Seznam.Add(Knedlik);
 Seznam.Add(Knedlik);
 Seznam.Add(Knedlik);
 readln;
end.
