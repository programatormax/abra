﻿program Spravne;

{$APPTYPE CONSOLE}

{$R *.res}

uses

  System.SysUtils;

type


  IMyInterface = interface
    procedure P1;
  end;

  TMyClass = class(TInterfacedObject, IMyInterface)
    procedure P1;
  end;


procedure TMyClass.P1;
begin
  writeln('Hello word');
end;

var

  MyClass: TMyClass;
  MyInterface: IMyInterface;


//Kdyby to bylo aplikace s tlačítkem
//procedure TForm3.Button1Click(Sender: TObject);
begin

/////////////////////////////////////////
/// Nefunkční verze
////////////////////////////////////////

  //MyClass := TMyClass.Create;
  //MyInterface := MyClass;   //Nyní třída MyClass patří do MyInterface to znamena dojde k chybě...(Neplatná operace s Pointer)
  //MyInterface.P1;
  //MyClass.Free;             //Tady je CHYBA!. Uvolnil jsi paměť, která uz nepatři MyClass.


/////////////////////////////////////////
/// funkční verze 1
////////////////////////////////////////

  //MyClass := TMyClass.Create;
  //MyInterface := MyClass;
  //MyInterface.P1;
  //Exit;

/////////////////////////////////////////
/// funkční verze 2
////////////////////////////////////////

  MyInterface:=TMyClass.Create;
  MyInterface.P1;

  readln;
end.
