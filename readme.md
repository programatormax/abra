**VSTUPNÍ TEST**
**MAXIM STARTSEV**


**1. Jaké číslo následuje v následující sekvenci čísel? (2b)**

   32/2, 8/1, 16/4, 2, 1, 1/2, 16/64, ___

   výsledek je 16/128 protože:
  
   ....   = atd
   32/2   = 16
   8/1    = 8
   16/4   = 4
   2/1    = 2
   1/1    = 1
   1/2    = 0.500
   16/64  = 0.250
 [16/128] = 0.125
   ....   = 0.0625 
   ....   = atd
   
**2. Jaký je výsledek? (2b)**
   
   (-6)(-6)+7-2(-6)-1 = ___

   výsledek: 54  
   
   https://gitlab.com/programatormax/abra/-/blob/main/2/Check.dpr

**3. Jakou hodnotu budou mít proměnné A, B, C, D a E po průchodu následujícího zdrojového kódu? (5b)**

   A = 120
   B = 4
   C = 1
   D = -7
   E = 0
   FOR I = 1 TO A STEP 1
        C = C + 1
        IF I = B
              C = C - 4
        ENDIF
        D = D - 1
   ENDFOR

   https://gitlab.com/programatormax/abra/-/blob/main/3/Check.dpr

   A: 120
   B: 4
   C: 117
   D: -127
   E: 0

**4. Napište SQL dotaz, který vrátí seznam čísel dokladů, které mají více jak 5 řádků. (5b)**

   SELECT DocumentID FROM Rows WHERE PosIndex>5 

**5. Napište SQL dotaz, který vrátí číslo dokladu(jednoho), který má celkovou částku nejvíce se blížící 
   průměrné ceně dokladu určené ze všech dokladů, které mají sudé číslo dokladu. (10b)**

   select DocumentID from Rows where Amount < (select avr(Amount) from Rows where mod(DocumentId,2)=0 ) limit = 1;

**6. Jaký je rozdíl mezi objektem a recordem (záznamem)? (1b)**

                                  
Konstruktor a destruktor tříd     RECORD ( NE )   OBJECT ( ANO )
Zapouzdření (Encapsulation)       RECORD ( NE )   OBJECT ( ANO )
Pozůstalost (Inheritance)         RECORD ( NE )   OBJECT ( ANO )
Polymorfismus (virtual methods)   RECORD ( NE )   OBJECT ( ANO )
Operator overload (global)        RECORD ( NE )   OBJECT ( ANO )


**7. K čemu se používají v definici třídy následující klíčová slova private, protected, 
   public a published, strict private, strict protected?
   Čím se od sebe liší private a strict private? (2b)**

private - proměnné a metody z privátní sekce jsou přístupné pouze v modulu.
protected - stejné jako private, až na to, k dispozici potomkům tohoto typu, i když jsou používáni v jiných modulech.
public - je vždy k dispozici.
published - stejné jako public, ale kompilátor generuje metadata RTTI (Run-Time Type Information)
strict private - ke všem metodám, které jsou z přísně strict private, lze přistupovat pouze z metod samotné třídy.
strict protected - členové třídy jsou viditelní pouze pro třídu a její potomky.


POZNAMKA:

Sekce private, protected, strict private, strict protected a veřejný se od sebe liší pouze textem kódu Delphi 
(tj. jejich implementace je čistě na vysoké úrovni). Během konečné kompilace třídy do assembleru jsou tyto sekce ignorovány.
 
**8. Když nadefinujete třídu bez klíčového slova z předchozího bodu, 
   jaká varianta bude implicitní? (2b)**

   Pokud není zadáne klíčového slova je výchozí hodnota published (pokud je třída zkompilována pomocí direktivy {$M+}) 
   jinak mají takové komponenty atribut public.


**9. K čemu se používá klíčové slovo „abstract“? (2b)**

   Je abstraktní znamena nemá žádnou implementaci v aktuální třídě, ale musí být implementována podřízenými třídami.

**10. Pomocí jaké property komponenty se nastavuje vzájemné pořadí prvků pro editaci? (1b)**

    index,read,write

    příklad:
    property Left: Longint index 0 read GetCoordinate write SetCoordinate;
    property Top: Longint index 1 read GetCoordinate write SetCoordinate;
    property Right: Longint index 2 read GetCoordinate write SetCoordinate;
    property Bottom: Longint index 3 read GetCoordinate write SetCoordinate


**11. Jaké jsou v Delphi datové typy pro řetězce? (2b)**
  
    String
    AnsiString
    WideString
    ShortString
    PChar     
    PWideChar  

12. **Díky čemu funguje konstrukce For...in? (2b)**

     V Delphi for..in loop nastavuje pořadí výčtu.

     https://gitlab.com/programatormax/abra/-/tree/main/12

13. **Uveďte jednoduchý příklad použití generické třídy (2b)**
  
    https://gitlab.com/programatormax/abra/-/blob/main/13/Example.dpr

**14. Jak jsou inicializované lokální proměnné ve funkcích? (2b)**

    function DoSomethig: UInt64;
      type
        TExample = record
          P1: Pointer;
          P2: UInt64;
        end;

      end;
    var 
      A: String; B: Integer; C: Boolean, D: TExample; 
    begin
      Result:=0;
      A:='ONE';
      B:=2;
      C:=True;
    end; 
    

**15. Jak jsou inicializovány členy (members, proměnné) třídy? (2b)**

function DoSomethig: UInt64;
type

  TExample = record
   P1: Pointer;
   P2: UInt64;
  end;
  PExample = ^TExample;

var

  A: String;
  B: Integer;
  C: Boolean;
  D: TExample;
  L: PExample;
  O: TObject;

begin
  GetMem(L,SizeOf(TExample));
  O:=TObject.Create;
  //...
  //...
  O.Destroy;
  O:=nil;
  FreeMem(L);
  readln;
end;

var

   O: TObject;

begin
  O:=TObject.Create;
  //...
  //...
  O.Destroy;
  O:=nil;
end;

**16. Napište co nejvíce příkladů, jak lze předčasně ukončit funkci (5b)**

    https://gitlab.com/programatormax/abra/-/blob/main/16/Priklady.dpr

**17. Napište automatické testy na následující funkci (5b)**

    https://gitlab.com/programatormax/abra/-/blob/main/17/TestInc.dpr

**18. Máme následující ukázku kódu.**

    a) Proč příklad vyvolá exception „Invalid pointer operation“? (5b)

        MyClass := TMyClass.Create;
        MyInterface := MyClass;   //Nyní třída patří do Interfacu to znamena dojde k chybě...(Uvolnis  paměť, která ti nepatřila)
        MyInterface.P1;
        MyClass.Free;             //CHYBA!. Uvolnil jsi paměť, která ti nepatřila.(Nyní třída patří do Interfacu)
         
    b) Napište příklad správně.

    https://gitlab.com/programatormax/abra/-/blob/main/18/Spravne.dpr

    
**19. Napište v jazyce Delphi proceduru, která má vstup AInput: TList<Integer> a vypíše na std. výstup následující.
    a) nejčastěji se vyskytující číslo (5b)
    b) hodnotu mediánu (5b)**

    https://gitlab.com/programatormax/abra/-/blob/main/19/Calc.dpr



**Podpisem tohoto formuláře souhlasí uchazeč se zpracováním osobních údajů podle zákona č. 101/2000 Sb. 
Firmou ABRA Software a.s. pro její vlastní potřeby.**

Datum: 10.05.2022				Podpis: MAXIM STARTSEV

 Souhlasím s tím


