﻿program TestInc;

{$APPTYPE CONSOLE}

uses
  SysUtils;

var

  I: Integer;
  Counter: UInt64;
  A,B,C,D,EX,EN: Integer;

function Inc(A, B: Integer): Integer;
begin
  if A < 0 then
    raise Exception.Create('Parametr A nesmí být záporné číslo');
  Result := A + B;
end;

begin
  Counter:=104;
  A:=100;
  B:=0;
  EX:=0;
  EN:=0;
  writeln('=========================================');
  writeln('           TEST FUNCTION INC             ');
  writeln('=========================================');
  writeln(' Counter: '+ IntToStr(Counter));
  writeln(' Initial parametr A: '+ IntToStr(A));
  writeln(' Initial parametr B: '+ IntToStr(B));
  writeln('=========================================');
  for I:=0 to Counter-1 do
  begin
    try
      C:=A+B;
      D:=INC(A,B);
      if C=D then
      begin
        writeln('[ výborně ] A: '+IntToStr(A)+' B: '+IntToStr(B)+' očekávam: '+IntToStr(C)+' result: '+IntToStr(D));
      end
      else
      begin
        writeln('[ nesouhlasí ] A: '+IntToStr(A)+' B: '+IntToStr(B)+' očekávam: '+IntToStr(C)+' result: '+IntToStr(D));
         EN:=EN+1;
      end;
    except
      on E: Exception do
      begin
        writeln('[ chyba ] A: '+IntToStr(A)+' B: '+IntToStr(B)+' očekávam: '+IntToStr(C)+' result: '+E.Message);
        EX:=EX+1;
      end;
    end;
    A:=A-1;
    B:=B+1;
  end;
  writeln('=========================================');
  writeln(' chyba: '+IntToStr(EX)+' nesouhlasí: '+IntToStr(EN));
  writeln('=========================================');
  readln;
end.
