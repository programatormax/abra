program Priklady;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils;

function Str1: String;
var a: Integer;
begin
  Result:='Good';
  randomize;
  for a:=0 to 100 do
  begin
    if random(100)=5 then
    begin
      Result:='Break';
      break;
    end;
  end;
end;

function Str2: String;
var a: Integer;
begin
  randomize;
  for a:=0 to 100 do
  begin
    if random(100)=5 then
    begin
      Result:='Break1';
      Exit;
    end;
    if random(100)=20 then
    begin
      Result:='Break2';
      Exit;
    end;
  end;
  Result:='Good';
end;

function Str3: String;
label ohmygood;
var a: Integer;
begin
  randomize;
  for a:=0 to 100 do
  begin
    if random(100)=5 then
    begin
      Result:='Break';
      goto ohmygood;
    end;
  end;
  Result:='Good';
 ohmygood:
end;


function Str4: String;
var a: Integer;
begin
  randomize;
  try
    for a:=0 to 100 do
    begin
      if random(100)=3 then raise Exception.Create('Break1');
      if random(100)=5 then raise Exception.Create('Break2');
    end;
    Result:='Good';
  except
    on E: Exception do
    begin
      Result:=E.Message;
    end;
  end;
end;

begin
  writeln('priklady');
  writeln('1: '+Str1);
  writeln('2: '+Str2);
  writeln('3: '+Str3);
  writeln('4: '+Str4);
  readln;
end.
