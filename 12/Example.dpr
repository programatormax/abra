program Example;

{$APPTYPE CONSOLE}

{$R *.res}


var
  a, b: word;

begin
  a:=0;
  for b in [3,5,10,20] do
  begin
    a:=a+1;
    writeln('A: ', a, ',B: ', b);
  end;
  readln;
  //A: 1, B: 3
  //A: 2, B: 5
  //A: 3, B: 10
  //A: 4, B: 20

end.

