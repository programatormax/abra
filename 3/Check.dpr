program Check;

{$APPTYPE CONSOLE}

{$R *.res}

//A = 120
//B = 4
//C = 1
//D = -7
//E = 0
//FOR I = 1 TO A STEP 1
//	C = C + 1
//	IF I = B
//		C = C � 4
//	ENDIF
//	D = D - 1
//ENDFOR

//A: 120
//B: 4
//C: 117
//D: -127
//E: 0

uses
  System.SysUtils;

var
  A,B,C,D,E,I: Integer;

begin
  A:=120;
  B:=4;
  C:=1;
  D:=-7;
  E:=0;
  for I:= 1 to A do
  begin
    C:=C+1;
    if I=B then C:=C-4;
    D:=D-1;
  end;
  writeln('A: '+IntToStr(A));
  writeln('B: '+IntToStr(B));
  writeln('C: '+IntToStr(C));
  writeln('D: '+IntToStr(D));
  writeln('E: '+IntToStr(E));
  readln;
end.
